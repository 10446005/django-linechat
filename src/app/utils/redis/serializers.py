import orjson

from django_redis.serializers.base import BaseSerializer


class JSONSerializer(BaseSerializer):

    def dumps(self, value):
        return orjson.dumps(value, option=orjson.OPT_INDENT_2)

    def loads(self, value):
        return orjson.loads(value)