import uuid

from django.core.files.base import ContentFile
from django.core.files.storage import default_storage, Storage


def upload(data, basename=None, extension=None):
    bucket: Storage = default_storage
    # generate blob name
    blob_name = basename or uuid.uuid4().hex
    if extension:
        blob_name += "." + extension
    # create the blob
    path = default_storage.save(blob_name, ContentFile(data))
    return bucket.url(path)
