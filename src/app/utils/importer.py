# https://github.com/encode/databases/blob/master/databases/importer.py
import importlib
import typing


class ImportFromStringError(Exception):
    pass


def import_module_from_string(import_str: str) -> typing.Any:
    try:
        module = importlib.import_module(import_str)
        return module
    except ImportError as exc:
        if exc.name != import_str:
            raise exc from None
        message = 'Could not import module "{module_str}".'
        raise ImportFromStringError(message.format(module_str=import_str))


def import_from_string(import_str: str) -> typing.Any:
    module_str, _, attrs_str = import_str.rpartition(".")
    if not module_str or not attrs_str:
        message = (
            'Import string "{import_str}" must be in format "<module>.<attribute>".'
        )
        raise ImportFromStringError(message.format(import_str=import_str))
    instance = import_module_from_string(module_str)
    try:
        for attr_str in attrs_str.split("."):
            instance = getattr(instance, attr_str)
    except AttributeError as exc:
        message = 'Attribute "{attrs_str}" not found in module "{module_str}".'
        raise ImportFromStringError(
            message.format(attrs_str=attrs_str, module_str=module_str)
        )

    return instance
