from .services import StateService


# STATE_INFO = {
#     'init': 'default',
#     'states': ['default', 'hello']
# }


def default(service: StateService, message: dict):
    service.add_message({
        "type": "text",
        "text": "This is `default` state"
    })
    service.add_message(message)
    service.switch_state('hello')
    service.finish_state({
        "type": "text",
        "text": "Switch to `hello` state"
    })


def hello(service: StateService, message: dict):
    service.add_message({
        "type": "text",
        "text": "This is `hello` state"
    })
    service.add_message(message)
    service.add_message({
        "type": "text",
        "text": "Hello world !!"
    })
    service.add_message({
        "type": "text",
        "text": "XXXXXXXX"
    })
    service.add_message(message)
    service.finish_state()
    service.switch_state('default')
