from django.db import models
from . import base as router_models


class LineChannel(router_models.Channel):
    access_token = models.CharField(max_length=255)
    channel_secret = models.CharField(max_length=255)

    def save(self, *args, **kwargs):
        if not self.created:
            self.type = router_models.PlatformChoice.LINE
        super().save(*args, **kwargs)


class LineUser(router_models.BotUser):
    user_id = models.CharField(max_length=255)

    def save(self, *args, **kwargs):
        if not self.created:
            self.type = router_models.PlatformChoice.LINE
        super().save(*args, **kwargs)