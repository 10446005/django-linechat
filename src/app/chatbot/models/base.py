from django.db import models
from app.utils.models import BaseModel


class PlatformChoice(models.TextChoices):
    FACEBOOK = "Facebook", "Facebook"
    LINE = "Line", "Line"
    WECHAT = "WeChat", "WeChat"
    TELEGRAM = "Telegram", "Telegram"


# Channel model
class Channel(BaseModel):
    name = models.CharField(max_length=255)
    default_bot = models.ForeignKey(
        to="Bot",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name="+",
    )
    type = models.CharField(
        max_length=255,
        editable=False,
        choices=PlatformChoice.choices,
    )

    def __str__(self):
        return self.name


# Bot model
class Bot(BaseModel):
    name = models.CharField(max_length=255)
    webhook = models.URLField()
    secret_key = models.CharField(max_length=255, default="", blank=True)

    def __str__(self):
        return self.name


class BotUser(BaseModel):
    channel = models.ForeignKey(Channel, on_delete=models.CASCADE)
    type = models.CharField(
        max_length=255,
        editable=False,
        choices=PlatformChoice.choices
    )
    # connecting bot of the user
    current_bot = models.ForeignKey(
        to=Bot, null=True, blank=True, default=None, on_delete=models.CASCADE
    )

    def save(self, *args, **kwargs):
        if not self.current_bot:
            self.current_bot = self.channel.default_bot
        super().save(*args, **kwargs)
