import logging
from django.contrib import admin, messages
from django.http.request import HttpRequest

from . import models


class BaseAdmin(admin.ModelAdmin):
    list_display = ("id", "modified")
    ordering = ("-modified",)


class BotAdmin(BaseAdmin):
    list_display = ("name", "modified")


class ChannelAdmin(BaseAdmin):
    list_display = ("name", "webhook_path", "modified")

    def get_queryset(self, request: 'HttpRequest'):
        self.request = request
        return super().get_queryset(request)

    def webhook_path(self, instance: 'models.Channel') -> str:
        path = "/chatbot/channel/{}".format(instance.id)
        webhook_url = str(
            self.request.build_absolute_uri(path)
        ).replace('http', 'https')
        return webhook_url

    webhook_path.short_description = "Web hook URL"


class UserAdmin(BaseAdmin):
    list_display = ("id", "current_bot")


admin.site.register(models.LineChannel, ChannelAdmin)
admin.site.register(models.LineUser, UserAdmin)
