from django import urls

from . import views

urlpatterns = [
    urls.path("channel/<str:channel_id>", views.webhook),
]