import typing

from abc import ABC, abstractmethod

from django.http.request import HttpRequest

from app.chatbot import models


# channel client interface
class ChannelClient(ABC):
    @abstractmethod
    def __init__(self, channel: 'models.Channel') -> 'None':
        """
        Initialize the channel client
        """
        raise NotImplementedError('"__init__" is not implemented')

    @abstractmethod
    def send_message(self, recipient_id: 'models.BotUser', message: 'typing.Union[list, dict]') -> 'bool':
        """Send a message to specified recipient

        Args:
            recipient_id: ID of a User instance
            message: a Taro message

        Returns:
            An object which is interpreted as True when the message is
            successfully sent, otherwise one interpreted as False
        """
        raise NotImplementedError('"send_message" is not implemented')

    @abstractmethod
    def authenticate_request(self, request: 'HttpRequest') -> 'bool':
        """Authenticate the API request

        Args:
            request: a HttpRequest instance

        Returns:
            True if the request is sent by the channel owner, otherwise False
        """
        raise NotImplementedError('"authenticate_request" is not implemented')

    def broadcast_message(self, message: 'typing.Union[list, dict]') -> 'typing.Union[str, None]':
        """Broadcast the message to all user

        Args:
            message: a Taro message

        Returns:
            A str object which gives extra infomation about braodcasting
        """
        raise NotImplementedError('"broadcast_message" is not implemented')

    @abstractmethod
    def normalize_message(self, data: 'dict') -> dict:
        raise NotImplementedError('"normalize_message" is not implemented')

    @abstractmethod
    def parse_message(self, message: 'dict') -> dict:
        raise NotImplementedError('"normalize_message" is not implemented')


# bot client interface
class BotClient:
    def __init__(self, bot_id):
        """Initialize the bot client

        Args:
            bot: ID of a Bot instance

        Returns:
            None
        """
        raise NotImplementedError('"__init__" is not implemented')

    def send_event(self, event_type, **kwargs):
        """Send a event to the bot

        Args:
            event_type: a str instance
            kwargs: additional infomations bundled with the event

        Returns:
            An object which is interpreted as True when the message is
            successfully sent, otherwise one interpreted as False
        """
        raise NotImplementedError('"send_event" is not implemented')
