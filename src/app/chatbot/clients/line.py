import io
import linebot
import pydub
import requests

from collections.abc import Collection

from app.utils import storage

from .. import models
from .base import ChannelClient


# Client implementation for Line
class LineClient(ChannelClient):
    def __init__(self, channel: 'models.LineChannel'):
        self.channel = channel
        self.api_client = linebot.LineBotApi(self.channel.access_token)

    def send_message(self, recipient_id, message):
        recipient = models.LineUser.objects.get(id=recipient_id)
        if isinstance(message, Collection):
            parsed_message = [self.parse_message(msg) for msg in message]
        else:
            parsed_message = self.parse_message(message)
        if parsed_message:
            self.api_client.push_message(
                to=recipient.user_id, messages=parsed_message
            )
            return True
        else:
            return False

    def authenticate_request(self, request):
        return (
                request.META.get("HTTP_AUTHORIZATION")
                == f"Bearer {self.channel.access_token}"
        )

    def broadcast_message(self, message):
        if isinstance(message, Collection):
            parsed_message = [self.parse_message(msg) for msg in message]
        else:
            parsed_message = self.parse_message(message)
        if not parsed_message:
            raise RuntimeError("message parsing failed")
        self.api_client.broadcast(parsed_message)

    # parse Taro message into Line message
    def parse_message(self, message):
        type = message["type"]
        if type == "text":
            return linebot.models.TextSendMessage(text=message["text"])
        elif type == "image":
            return linebot.models.ImageSendMessage(
                original_content_url=message["image"]["url"],
                preview_image_url=message["image"]["url"],
            )
        elif type == "audio":
            audio = pydub.AudioSegment.from_file(
                io.BytesIO(requests.get(message["audio"]["url"]).content)
            )
            url = storage.upload(audio.export(format="mp4").read(), extension="m4a")
            return linebot.models.AudioSendMessage(
                original_content_url=url, duration=audio.duration_seconds * 1000
            )
        elif type == "video":
            return linebot.models.VideoSendMessage(
                original_content_url=message["video"]["url"],
                preview_image_url="https://via.placeholder.com/240x240",
            )
        elif type == "sticker":
            if message["sticker"]["platform"] == "line" and (
                    1 <= int(message["sticker"]["package_id"]) <= 4
                    or 11537 <= int(message["sticker"]["package_id"]) <= 11539
            ):
                return linebot.models.StickerSendMessage(
                    package_id=message["sticker"]["package_id"],
                    sticker_id=message["sticker"]["sticker_id"],
                )
            else:
                return {}
        elif type == "location":
            return linebot.models.LocationSendMessage(
                title="位置分享",
                address=message["location"]["address"],
                latitude=message["location"]["latitude"],
                longitude=message["location"]["longitude"],
            )
        elif type == "button_template":
            return linebot.models.TemplateSendMessage(
                alt_text=message["button_template"]["text"],
                template=linebot.models.ButtonsTemplate(
                    text=message["button_template"]["text"],
                    actions=[
                        linebot.models.PostbackAction(
                            label=button["title"], data=button["payload"]
                        )
                        for button in message["button_template"]["buttons"]
                    ],
                ),
            )
        else:
            return {}

    def normalize_message(self, event: 'dict') -> dict:
        if event['type'] == 'message':
            data = event['message']
        else:
            data = event
        if data["type"] == "text":
            parsed_message = {"type": "text", "text": data["text"]}
        elif data["type"] in ("image", "audio", "video"):
            content = self.api_client.get_message_content(data["id"]).content
            if data["type"] == "image":
                extension = "jpg"
            elif data["type"] == "audio":
                extension = "m4a"
            elif data["type"] == "video":
                extension = "mp4"
            else:
                extension = None
            parsed_message = {
                "type": data["type"],
                data["type"]: {"url": storage.upload(content, extension=extension)},
            }
        elif data["type"] == "location":
            parsed_message = {
                "type": "location",
                "location": {
                    "address": data["address"],
                    "latitude": data["latitude"],
                    "longitude": data["longitude"],
                },
            }
        elif data["type"] == "sticker":
            parsed_message = {
                "type": "sticker",
                "sticker": {
                    "platform": "line",
                    "package_id": data["packageId"],
                    "sticker_id": data["stickerId"],
                },
            }
        elif data["type"] == "postback":
            parsed_message = {
                "type": "postback",
                "postback": {"payload": data["postback"]["data"]},
            }
        else:
            parsed_message = {}
        return parsed_message
