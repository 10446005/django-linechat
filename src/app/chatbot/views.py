from time import sleep

import linebot
import orjson
import threading

from functools import partial

from django import http
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from linebot.exceptions import LineBotApiError

from . import models, services, clients, utils

LINE_SERVER_VERIFY_USER_ID = 'Udeadbeefdeadbeefdeadbeefdeadbeef'


def handle_event(channel, client, event, time):
    sleep(time)
    print(f"waiting.....{time}")
    user_id = event["source"]["userId"]
    if user_id == LINE_SERVER_VERIFY_USER_ID:
        return
    source_user, _ = models.LineUser.objects.get_or_create(
        channel=channel, user_id=user_id
    )
    service = services.StateService(client, source_user)
    service.update_reply_token(event["replyToken"], 4)
    service.setup_state('default')
    try:
        is_finish = service.handle_state(event)
        if not is_finish:
            return
        result = service.get_reply_or_send(can_push=False, max_content_num=3)
        if result:
            client.api_client.reply_message(service.reply_token, messages=result)
    except Exception:
        service.clear_caches()
        return http.HttpResponseBadRequest()


@require_http_methods(["GET", "POST"])
@csrf_exempt
def webhook(request, *args, **kwargs):
    # verify the signature
    channel = models.LineChannel.objects.get(id=kwargs["channel_id"])
    client = clients.LineClient(channel)
    parser = linebot.WebhookParser(channel.channel_secret)
    signature = request.META.get("HTTP_X_LINE_SIGNATURE", None)
    try:
        parser.parse(request.body.decode(), signature)
    except linebot.exceptions.InvalidSignatureError as e:
        return http.HttpResponse(str(e), status=401)
    # handle each event
    handle_event_func = partial(handle_event, channel, client)
    webhook_event = orjson.loads(request.body.decode('utf-8'))

    count = 1
    for event in webhook_event["events"]:
        th = threading.Thread(target=handle_event_func, args=(event, count*25))
        th.start()
        count += 1
    return http.HttpResponse('ok', status=200)
