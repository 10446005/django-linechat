import typing
import enum

from django.core.cache import cache
from app.utils.importer import import_module_from_string
from . import clients, models


class FixedCommand(enum.Enum):
    GET_MORE_MESSAGES = 'GET_MORE_MESSAGES'


def make_cache_key_map(cache_prefix: str) -> dict:
    return {
        # bot user state
        "state": f"state__{cache_prefix}",
        # lock handle state
        "in_progress": f"in_progress__{cache_prefix}",
        "reply_token": f"reply_token__{cache_prefix}",
        "result_data": f"result_data__{cache_prefix}",
    }


def _get_state_fn(state: str, handler_module: typing.Any):
    return getattr(handler_module, state)


class StateService:
    def __init__(self, client: 'clients.ChannelClient', bot_user: 'models.BotUser'):
        self.cache_prefix: str = f"{bot_user.id}"
        self.cache_key_map: dict = make_cache_key_map(self.cache_prefix)
        self.client = client
        self.user = bot_user
        self.handler_module = import_module_from_string('app.chatbot.handler')
        result_data = cache.get_or_set(self.cache_key_map['result_data'], [], 60*60)
        self.result: typing.List[typing.Any] = result_data
        self.cache_data = {}

    def clear_caches(self):
        cache.delete_pattern(f"*__{self.cache_prefix}")

    @property
    def reply_token(self) -> typing.Union[None, str]:
        key = self.cache_key_map['reply_token']
        return cache.get(key)

    @property
    def is_in_progress(self) -> bool:
        is_in_progress = self.cache_key_map['in_progress']
        result = cache.get(is_in_progress)
        print(result)
        return bool(result) or False

    def setup_state(self, state: str) -> 'None':
        key = self.cache_key_map['state']
        val = cache.get(key)
        if val is None:
            # timeout 30 min
            cache.add(key, state, timeout=60*30)
        else:
            # incremented timeout 1 min
            cache.touch(key, timeout=60*30)

    def update_reply_token(self, reply_token: str, timeout=25) -> 'None':
        key = self.cache_key_map['reply_token']
        cache.set(key, reply_token, timeout)

    def _exec_state_fn(self, state: str, message: typing.Union[None, dict]) -> 'None':
        state_fn = _get_state_fn(state, self.handler_module)
        state_fn(self, message)

    def handle_state(self, event: dict) -> 'bool':
        if self.is_in_progress:
            return False
        message = self.client.normalize_message(event)
        if 'type' not in message.keys():
            print(message)
            return False
        if message.get('type') == 'postback':
            data = message['postback']["payload"]
            if FixedCommand[data] == FixedCommand.GET_MORE_MESSAGES:
                return True
        # timeout 1 hour
        cache.add(self.cache_key_map['in_progress'], True, timeout=60*60)
        state = cache.get(self.cache_key_map['state'])
        self._exec_state_fn(state, message)
        return True

    def switch_state(self, state: str):
        cache.set(self.cache_key_map['state'], state)

    def next_state(self, state: str, message: typing.Union[None, dict] = None):
        self.switch_state(state)
        self._exec_state_fn(state, message)

    def finish_state(self, message: typing.Union[None, dict] = None):
        self.add_message(message)
        cache.delete(self.cache_key_map['in_progress'])

    def add_message(self, message: typing.Union[None, dict]):
        if message:
            self.result.append(message)

    def _dumps_result(self, messages: list):
        self.result = messages
        cache.set(self.cache_key_map['result_data'], messages, 60 * 60)

    def get_reply_or_send(self,
                          can_push=True,
                          max_content_num=5,
                          can_show_more=True
                          ) -> typing.Union[None, typing.Any]:
        if len(self.result) == 0:
            return
        if not self.reply_token and can_push:
            self.client.send_message(self.user.id, self.result)
            return
        result_messages = self.result
        if can_show_more and len(self.result) > max_content_num:
            max_content_num -= 1
        reply_events = [self.client.parse_message(msg) for msg in result_messages[:max_content_num]]
        if can_show_more and len(self.result) > max_content_num:
            more_msg = {
                "type": "button_template",
                "button_template": {
                    "text": "您還有未顯示訊息",
                    "buttons": [
                        {"title": "顯示訊息", "payload": FixedCommand.GET_MORE_MESSAGES.value}
                    ]
                }
            }
            reply_events.append(self.client.parse_message(more_msg))
        self._dumps_result(result_messages[max_content_num:])
        return reply_events




