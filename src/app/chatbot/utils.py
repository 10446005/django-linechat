from django.http import HttpResponse


class AfterResponse(HttpResponse):
    def __init__(
        self,
        content=b"",
        handle_func=None,
        *args,
        **kwargs,
    ):
        self.handle_func = handle_func
        super().__init__(content, *args, **kwargs)

    def close(self):
        super().close()
        if self.handle_func:
            self.handle_func()