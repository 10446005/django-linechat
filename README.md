# Django LINE ChatBot

- python 3.7
- pipenv

tools:

- https://github.com/qishibo/AnotherRedisDesktopManager


## Install 

```shell
$ pipenv install --dev

$ cp .env.example src/config/.env
# or deploy
$ cp .env.example .env

$ cd dev-docker
$ docker-compose up -d

$ cd ..
$ pipenv shell
```

1. open http://127.0.0.1:9000
2. enter minioadmin: minioadmin
3. create bucket `files`


```shell
$ cd src
$ python manage.py shell < src/script/create_admin.py
$ python manage.py runserver
# or log sql
$ python manage.py runserver_plus --print-sql
```

1. open http://127.0.0.1:8000/admin
2. enter admin@mail.com: 0
3. http://127.0.0.1:8000/admin/chatbot/linechannel/
4. add linechannel and save
5. https://developers.line.biz/ setup webhook